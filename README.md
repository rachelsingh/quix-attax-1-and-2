# Quix Attax 1 and Quix Attax Deux

**Year:** 2006 and 2007

**Tools:** C++/Allegro

**Notes:** Quix Attax 1 was my first C++ game

## Screenshots

![Screenshot of game](screenshot-quix1.png)

Quix Attax 1

![Screenshot of game](screenshot-quix2.png)

Quix Attax Deux
